package ru.alegor.src2md;

import org.apache.commons.io.FilenameUtils;
import org.mozilla.universalchardet.UniversalDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    private static final String ARG_INPUT_CHARSET_PREFIX = "-inputCharset=";
    private static final String ARG_OUTPUT_CHARSET_PREFIX = "-outputCharset=";
    private static final String ARG_OUTPUT_PATH_PREFIX = "-output=";
    private static final String ARG_EXTENSION_PREFIX = ".";

    private static final String ARG_AUTO_DETECT_CHARSET = "auto";

    private static final String MARKDOWN_CODE_PREFIX = "    ";
    private static final String MARKDOWN_EMPTY_LINE = "";
    private static final String MARKDOWN_H1_PREFIX = "# ";
    private static final String MARKDOWN_H2_PREFIX = "## ";
    private static final String MARKDOWN_H3_PREFIX = "### ";

    private static final int BUFFER_SIZE = 8 * 1024 * 1024;

    public static void main(String[] args) throws Exception {
        File directory = null;
        List<String> errors = new ArrayList<>();
        List<String> extensions = new ArrayList<>();
        String outputFilename = String.format("src2md-out-%d.md", new Date().getTime() / 1000L);

        Charset inputCharset = Charset.defaultCharset();
        Charset outputCharset = Charset.defaultCharset();

        if (args.length > 0) {
            directory = new File(args[0]);
        }
        for (int i = 1; i < args.length; i++) {
            String arg = args[i];
            if (arg.startsWith(ARG_EXTENSION_PREFIX)) {
                extensions.add(arg);
            } else if (arg.startsWith(ARG_OUTPUT_PATH_PREFIX)) {
                outputFilename = arg.substring(ARG_OUTPUT_PATH_PREFIX.length());
            } else if (arg.startsWith(ARG_INPUT_CHARSET_PREFIX)) {
                String charsetName = arg.substring(ARG_INPUT_CHARSET_PREFIX.length());
                if (charsetName.equalsIgnoreCase(ARG_AUTO_DETECT_CHARSET)) {
                    inputCharset = null;
                } else if (Charset.isSupported(charsetName)) {
                    inputCharset = Charset.forName(charsetName);
                } else {
                    errors.add("Кодировка " + charsetName + " не поддерживается!");
                }
            } else if (arg.startsWith(ARG_OUTPUT_CHARSET_PREFIX)) {
                String charsetName = arg.substring(ARG_OUTPUT_CHARSET_PREFIX.length());
                if (Charset.isSupported(charsetName)) {
                    outputCharset = Charset.forName(charsetName);
                } else {
                    errors.add("Кодировка " + charsetName + " не поддерживается!");
                }
            }
        }

        if (directory == null) {
            errors.add("Не указан рабочий каталог!");
        } else if (directory.exists() && !directory.isDirectory()) {
            errors.add("Рабочий каталог не существует!");
        }
        if (extensions.isEmpty()) {
            errors.add("Не указаны расширения файлов!");
        }

        if (errors.size() > 0) {
            logger.error("Работа src2md завершилась с {} ошибками: {}{}",
                    errors.size(),
                    System.lineSeparator(),
                    String.join(System.lineSeparator(), errors));
            System.exit(1);
        } else {
            StringBuilder stringBuilder = new StringBuilder(System.lineSeparator());
            stringBuilder.append("Рабочий каталог: " + directory.getAbsolutePath() + System.lineSeparator());
            stringBuilder.append("Расширения: " + String.join(", ", extensions) + System.lineSeparator());
            stringBuilder.append("Кодировка исходных файлов: " + inputCharset + System.lineSeparator());
            stringBuilder.append("Выходной файл: " + outputFilename + System.lineSeparator());
            stringBuilder.append("Кодировка выходного файла: " + outputCharset + System.lineSeparator());
            logger.info("{}", stringBuilder);
        }

        Application application = new Application(directory, extensions, outputFilename, inputCharset, outputCharset);
        try {
            application.run();
        } catch (Exception e) {
            logger.error("Работа src2md завершилась с ошибкой!", e);
            System.exit(1);
        }

        System.exit(0);
    }

    private File directory;
    private List<String> extensions;
    private String outputFilename;
    private List<String> lines;
    private Charset inputCharset;
    private UniversalDetector universalDetector;
    private Charset outputCharset;

    public Application(File directory, List<String> extensions, String outputFilename, Charset inputCharset, Charset outputCharset) {
        this.directory = directory;
        this.extensions = extensions;
        this.outputFilename = outputFilename;
        this.inputCharset = inputCharset;
        universalDetector = inputCharset == null ? new UniversalDetector(null) : null;
        this.outputCharset = outputCharset;
        this.lines = new ArrayList<>();
    }

    public void run() throws Exception {
        scanDirectory(directory);

        try {
            writeResultFile(outputFilename, lines);
        } catch (Exception e) {
            logger.error("Запись файла с исходным кодом завершилась с ошибкой!", e);
            System.exit(1);
        }
    }

    private void scanDirectory(File directory) throws IOException {
        logger.info("{} - сканирование каталога", directory);
        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                scanDirectory(file);
                continue;
            }
            String extension = FilenameUtils.getExtension(file.getName());
            if (extensions.contains("." + extension)) {
                logger.info("{} - чтение файла", file);
                readFile(file);
            } else {
                logger.info("{} - пропуск файла", file);
            }
        }
    }

    private void readFile(File file) throws IOException {
        Charset inputCharset;

        if (universalDetector != null) {

            FileInputStream fis = new FileInputStream(file);

            universalDetector.reset();
            byte[] buffer = new byte[BUFFER_SIZE];
            int readed;
            while ((readed = fis.read(buffer)) > 0 && !universalDetector.isDone()) {
                universalDetector.handleData(buffer, 0, readed);
            }
            universalDetector.dataEnd();

            String detectedCharsetName = universalDetector.getDetectedCharset();
            if (detectedCharsetName == null) {
                inputCharset = Charset.defaultCharset();
                logger.info("Кодировка файла {} не определена. Используется кодировка по умолчанию: {}", inputCharset);
            } else {
                logger.info("Кодировка файла {} определена как {}", detectedCharsetName);
                inputCharset = Charset.forName(detectedCharsetName);
            }
        } else {
            inputCharset = this.inputCharset;
        }

        FileInputStream fileInputStream = new FileInputStream(file);

        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, inputCharset);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String relativePath = file.getAbsolutePath().substring(directory.getAbsolutePath().length() + 1);

        lines.add(MARKDOWN_H2_PREFIX + "Файл " + markdownNormalizeLine(relativePath));
        lines.add(MARKDOWN_EMPTY_LINE);

        for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
            lines.add(MARKDOWN_CODE_PREFIX + line);
        }

        lines.add(MARKDOWN_EMPTY_LINE);

        bufferedReader.close();
        fileInputStream.close();
    }

    private void writeResultFile(String fileName, List<String> lines) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, outputCharset);
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
        for (String line : lines) {
            bufferedWriter.write(line);
            bufferedWriter.newLine();
        }
        bufferedWriter.flush();
        fileOutputStream.close();
    }

    private String markdownNormalizeLine(String line) {
        //  Idea from http://meta.stackexchange.com/a/198231

        String resultLine = line
                .replace("\\", "\\\\")
                .replace("`", "\\`")
                .replace("*", "\\*")
                .replace("_", "\\_")
                .replace("{", "\\{")
                .replace("}", "\\}")
                .replace("[", "\\[")
                .replace("]", "\\]")
                .replace("(", "\\(")
                .replace(")", "\\)")
                .replace("#", "\\#")
                .replace("+", "\\+")
                .replace("-", "\\-")
                .replace(".", "\\.")
                .replace("!", "\\!");

        return resultLine;
    }
}